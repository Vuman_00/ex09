package com.vuman00.service;

import com.google.gson.Gson;
import com.vuman00.model.DAO.CarDAO;
import com.vuman00.model.entity.Car;
import com.vuman00.model.entity.MediumCar;
import com.vuman00.model.entity.ModermCar;
import com.vuman00.model.entity.OldCar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;

import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Optional;


@WebServlet(name = "api-car", urlPatterns = {"/api/cars/*"})
public class CarAPI extends HttpServlet {
    public Car mapJsonToCar(String json) {
        Gson gson = new Gson();
        try {
            final ModermCar modermCar = gson.fromJson(json, ModermCar.class);
            return modermCar;
        } catch (Exception ex) {
            try {
                final MediumCar mediumCar = gson.fromJson(json, MediumCar.class);
                return mediumCar;
            } catch (Exception ey) {
                try {
                    final OldCar oldCar = gson.fromJson(json, OldCar.class);
                    return oldCar;
                } catch (Exception ez) {
                    return null;
                }
            }
        }


    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        StringBuilder jsonBuff = new StringBuilder();
        String line = null;
        try {
            BufferedReader reader = request.getReader();
            while ((line = reader.readLine()) != null)
                jsonBuff.append(line);
        } catch (Exception e) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
        }
        Car car = mapJsonToCar(jsonBuff.toString());
        if (car != null) {
            if (new CarDAO().save(car)) {
                response.setStatus(HttpServletResponse.SC_CREATED);
            } else {
                response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
            }
        } else {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
        }
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String pathInfo = request.getPathInfo();

        if (pathInfo == null || pathInfo.equals("/")) {
            final ArrayList<Car> all = (ArrayList<Car>) new CarDAO().getAll();
            String json = new Gson().toJson(all);

            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(json);
            return;
        }

        String[] splits = pathInfo.split("/");
        if (splits.length != 2) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }
        try {
            int carId = Integer.parseInt(splits[1]);
            Optional<Car> car = new CarDAO().get(carId);
            if (car.isPresent()) {
                String json = new Gson().toJson(car.get());
                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
                response.getWriter().write(json);
            } else {
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
                return;
            }
        } catch (Exception ex) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        StringBuilder jsonBuff = new StringBuilder();
        String line = null;
        try {
            BufferedReader reader = request.getReader();
            while ((line = reader.readLine()) != null)
                jsonBuff.append(line);
        } catch (Exception e) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }
        Car car = mapJsonToCar(jsonBuff.toString());
        if (car != null)
        {
            if(new CarDAO().update(car))
                {
             response.setStatus(HttpServletResponse.SC_ACCEPTED);
            }
            else
                {
                response.sendError(HttpServletResponse.SC_FORBIDDEN);
            }
        } else {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
        }
    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String pathInfo = request.getPathInfo();

        if (pathInfo == null || pathInfo.equals("/")) {
        response.sendError(HttpServletResponse.SC_BAD_REQUEST);
        return;
        }
        String[] splits = pathInfo.split("/");
        if (splits.length != 2) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }
        try {
            int carId = Integer.parseInt(splits[1]);

            if(new CarDAO().delete(carId)){
                response.setStatus(HttpServletResponse.SC_OK);
            }
            else {
                response.sendError(HttpServletResponse.SC_FORBIDDEN);
            }

        } catch (Exception ex) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }

    }

}
