package com.vuman00.model.DAO;

import com.vuman00.model.entity.Car;
import com.vuman00.model.entity.Enum.Brand;
import com.vuman00.model.entity.MediumCar;
import com.vuman00.model.entity.ModermCar;
import com.vuman00.model.entity.OldCar;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CarDAO implements MAINDAO<Car> {
    private final String sqlinsert = "insert into car(carname,numberplate,yearmanufature,brand,haveisurance,actionduration,havepowersteering,havepositdevice) value(?,?,?,?,?,?,?,?)";
    private final String sqlgetall = "select * from car";
    private final String sqlgetbyid = "select * from car where idcar =?";
    private final String sqldelete = "delete from car where idcar = ?";
    private final String sqlgetcarnothaveinsurance = "select * from car where haveisurance=0";
    private final String sqldem = "select count(*) FROM car";
    private final String sqlupdate = "update car set haveisurance = ? where numberplate = ?";
    private final String sqlgetbynumberplate = "select * from car where numberplate = ?";

    //    public boolean insert(Car car) {
//
//        final Connection open = DBConection.open();
//        PreparedStatement ps = null;
//        boolean thanhcong = false;
//        try {
//            String carname = "Car " + executescalar();
//            ps = open.prepareStatement(sqlinsert);
//            ps.setString(1, carname);
//            ps.setString(2, car.getNumberPlate());
//            ps.setInt(3, car.getYearManufacture());
//            ps.setString(4, car.getBrand().toString());
//            ps.setBoolean(5, car.isHaveIsurance());
//            ps.setInt(6, 0);
//            ps.setBoolean(7, false);
//            ps.setBoolean(8, false);
//
//            int row = ps.executeUpdate();
//            if (row > 0) {
//                thanhcong = true;
//            }
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        } finally {
//            DBConection.close(null, ps, open);
//        }
//        return thanhcong;
//    }
//    public int executescalar() {
//        final Connection open = DBConection.open();
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        try {
//            ps = open.prepareStatement(sqldem);
//            rs = ps.executeQuery();
//            while (rs.next()) {
//                return rs.getInt(1);
//            }
//
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        } finally {
//            DBConection.close(null, ps, open);
//        }
//        return 0;
//    }
    @Override
    public Optional<Car> get(int id) {
        final Connection open = DBConection.open();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = open.prepareStatement(sqlgetbyid);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                int idcar = rs.getInt("idcar");
                String carname = rs.getString("carname");
                String numberplate = rs.getString("numberplate");
                int namsanxuat = rs.getInt("yearmanufature");
                String loai = rs.getString("brand");
                boolean cobaohiem = rs.getBoolean("haveisurance");
                Brand brand = Brand.BMW;
                switch (loai) {
                    case "TOYOTA":
                        brand = Brand.TOYOTA;
                        break;
                    case "HUYNDAI":
                        brand = Brand.HUYNDAI;
                        break;
                    default:
                        brand = Brand.BMW;
                }
                if (namsanxuat <= 1995) {
                    OldCar car = new OldCar();
                    car.setIdcar(idcar);
                    car.setCarname(carname);
                    car.setNumberPlate(numberplate);
                    car.setYearManufacture(namsanxuat);
                    car.setBrand(brand);
                    car.setHaveIsurance(cobaohiem);
                    car.setActionDuration(rs.getInt("actionduration"));
                    return Optional.of(car);

                } else if (namsanxuat >= 1996 && namsanxuat <= 2004) {
                    MediumCar car = new MediumCar();
                    car.setIdcar(idcar);
                    car.setCarname(carname);
                    car.setNumberPlate(numberplate);
                    car.setYearManufacture(namsanxuat);
                    car.setBrand(brand);
                    car.setHaveIsurance(cobaohiem);
                    car.setHavPowersteering(rs.getBoolean("havepowersteering"));
                    return Optional.of(car);
                } else if (namsanxuat >= 2005) {
                    ModermCar car = new ModermCar();
                    car.setIdcar(idcar);
                    car.setCarname(carname);
                    car.setNumberPlate(numberplate);
                    car.setYearManufacture(namsanxuat);
                    car.setBrand(brand);
                    car.setHaveIsurance(cobaohiem);
                    car.setHavePositdevice(rs.getBoolean("havepositdevice"));
                    return Optional.of(car);
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBConection.close(null, ps, open);
        }
        return Optional.empty();
    }

    @Override
    public List<Car> getAll() {
        final Connection open = DBConection.open();
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Car> list = null;
        try {
            ps = open.prepareStatement(sqlgetall);
            rs = ps.executeQuery();
            list = new ArrayList<>();
            while (rs.next()) {
                int idcar = rs.getInt("idcar");
                String carname = rs.getString("carname");
                String numberplate = rs.getString("numberplate");
                int namsanxuat = rs.getInt("yearmanufature");
                String loai = rs.getString("brand");
                boolean cobaohiem = rs.getBoolean("haveisurance");
                Brand brand = Brand.BMW;
                switch (loai) {
                    case "TOYOTA":
                        brand = Brand.TOYOTA;
                        break;
                    case "HUYNDAI":
                        brand = Brand.HUYNDAI;
                        break;
                    default:
                        brand = Brand.BMW;
                }
                if (namsanxuat <= 1995) {
                    OldCar car = new OldCar();
                    car.setIdcar(idcar);
                    car.setCarname(carname);
                    car.setNumberPlate(numberplate);
                    car.setYearManufacture(namsanxuat);
                    car.setBrand(brand);
                    car.setHaveIsurance(cobaohiem);
                    car.setActionDuration(rs.getInt("actionduration"));
                    list.add(car);
                } else if (namsanxuat >= 1996 && namsanxuat <= 2004) {
                    MediumCar car = new MediumCar();
                    car.setIdcar(idcar);
                    car.setCarname(carname);
                    car.setNumberPlate(numberplate);
                    car.setYearManufacture(namsanxuat);
                    car.setBrand(brand);
                    car.setHaveIsurance(cobaohiem);
                    car.setHavPowersteering(rs.getBoolean("havepowersteering"));
                    list.add(car);
                } else if (namsanxuat >= 2005) {
                    ModermCar car = new ModermCar();
                    car.setIdcar(idcar);
                    car.setCarname(carname);
                    car.setNumberPlate(numberplate);
                    car.setYearManufacture(namsanxuat);
                    car.setBrand(brand);
                    car.setHaveIsurance(cobaohiem);
                    car.setHavePositdevice(rs.getBoolean("havepositdevice"));
                    list.add(car);
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBConection.close(null, ps, open);
        }
        return list;
    }

    @Override
    public boolean save(Car car) {

        final Connection open = DBConection.open();
        PreparedStatement ps = null;
        boolean thanhcong = false;
        try {
            ps = open.prepareStatement(sqlinsert);
            ps.setString(1, car.getCarname());
            ps.setString(2, car.getNumberPlate());
            ps.setInt(3, car.getYearManufacture());
            ps.setString(4, car.getBrand().toString());
            ps.setBoolean(5, car.isHaveIsurance());
            if (car instanceof OldCar) {
                OldCar oldCar = (OldCar) car;
                ps.setInt(6, oldCar.getActionDuration());
                ps.setBoolean(7, false);
                ps.setBoolean(8, false);
            } else if (car instanceof MediumCar) {
                MediumCar mediumCar = (MediumCar) car;
                ps.setInt(6, 0);
                ps.setBoolean(7, mediumCar.isHavPowersteering());
                ps.setBoolean(8, false);
            } else {
                ModermCar modermCar = (ModermCar) car;
                ps.setInt(6, 0);
                ps.setBoolean(7, false);
                ps.setBoolean(8, modermCar.isHavePositdevice());
            }

            int row = ps.executeUpdate();

            if (row > 0) {
                thanhcong = true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBConection.close(null, ps, open);
        }
        return thanhcong;
    }

    @Override
    public boolean update(Car car) {
        final Connection open = DBConection.open();
        PreparedStatement ps = null;
        boolean thanhcong = false;
        String sqlupdatebyid="";
        if (car instanceof OldCar) {
               sqlupdatebyid = "update car set carname=?, numberplate=?,yearmanufature=?,brand=?, haveisurance = ? , actionduration = ? where idcar = ?";
        } else if (car instanceof MediumCar) {
              sqlupdatebyid = "update car set carname=?, numberplate=?,yearmanufature=?,brand=?, haveisurance = ? , havepowersteering = ? where idcar = ?";
        } else {
               sqlupdatebyid = "update car set carname=?, numberplate=?,yearmanufature=?,brand=?, haveisurance = ? , havepositdevice = ? where idcar = ?";
        }

        try {
            ps = open.prepareStatement(sqlupdatebyid);
            ps.setString(1, car.getCarname());
            ps.setString(2, car.getNumberPlate());
            ps.setInt(3, car.getYearManufacture());
            ps.setString(4, car.getBrand().toString());
            ps.setBoolean(5, car.isHaveIsurance());
            //update by car type
            if (car instanceof OldCar) {
                OldCar oldCar = (OldCar) car;

                ps.setInt(6, oldCar.getActionDuration());
            } else if (car instanceof MediumCar) {
                MediumCar mediumCar = (MediumCar) car;

                ps.setBoolean(6, mediumCar.isHavPowersteering());
            } else {
                ModermCar modermCar = (ModermCar) car;

                ps.setBoolean(6, modermCar.isHavePositdevice());
            }

            ps.setInt(7, car.getIdcar());

            int row = ps.executeUpdate();
            if (row > 0) {
                thanhcong = true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBConection.close(null, ps, open);
        }
        return thanhcong;

    }

    @Override
    public boolean delete(int idcar) {
        final Connection open = DBConection.open();
        PreparedStatement ps = null;
        boolean thanhcong = false;
        try {
            ps = open.prepareStatement(sqldelete);
            ps.setInt(1, idcar);

            int row = ps.executeUpdate();
            if (row > 0) {
                thanhcong = true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBConection.close(null, ps, open);
        }
        return thanhcong;

    }

}
