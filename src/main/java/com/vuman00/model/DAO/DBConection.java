package com.vuman00.model.DAO;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DBConection {

        public  static String url="jdbc:mysql://localhost:3306/ex02";
        public  static String user="admin";
        public  static String pass="123456";
        public  static String driver="com.mysql.jdbc.Driver";

        public static  Connection open()  {
            try {
                Class.forName(driver);
                Connection connection = DriverManager.getConnection(url, user, pass);
                System.out.println("Successful conected to mysql");
                return connection;
            } catch (SQLException ex) {
                Logger.getLogger(DBConection.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(DBConection.class.getName()).log(Level.SEVERE, null, ex);
            }
            return null;
        }

        public static void close(ResultSet rs, PreparedStatement ps, Connection con){
            try {
                if(rs!=null && !rs.isClosed()){
                    rs.close();
                }   } catch (SQLException ex) {
                Logger.getLogger(DBConection.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                if(ps!=null && !ps.isClosed()){
                    ps.close();
                }   } catch (SQLException ex) {
                Logger.getLogger(DBConection.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                if(con!=null && !con.isClosed()){
                    con.close();
                }   } catch (SQLException ex) {
                Logger.getLogger(DBConection.class.getName()).log(Level.SEVERE, null, ex);
            }
            System.out.println("Closed mysql");
        }


}




