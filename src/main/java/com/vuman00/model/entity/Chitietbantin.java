package com.vuman00.model.entity;

import java.util.Date;

public class Chitietbantin {
    private int idchitiet;
    private String tieude;
    private String noidung;
    private String tomtat;
    private int solanxem;
    private java.sql.Date ngaydang;
    private int iddanhmuc;
    private String lkanh;


    public String getTomtat() {
        return tomtat;
    }

    public void setTomtat(String tomtat) {
        this.tomtat = tomtat;
    }

    public int getIdchitiet() {
        return idchitiet;
    }

    public void setIdchitiet(int idchitiet) {
        this.idchitiet = idchitiet;
    }

    public String getTieude() {
        return tieude;
    }

    public void setTieude(String tieude) {
        this.tieude = tieude;
    }

    public String getNoidung() {
        return noidung;
    }

    public void setNoidung(String noidung) {
        this.noidung = noidung;
    }

    public int getSolanxem() {
        return solanxem;
    }

    public void setSolanxem(int solanxem) {
        this.solanxem = solanxem;
    }

    public Date getNgaydang() {
        return ngaydang;
    }

    public void setNgaydang(java.sql.Date ngaydang) {
        this.ngaydang = ngaydang;
    }

    public int getIddanhmuc() {
        return iddanhmuc;
    }

    public void setIddanhmuc(int iddanhmuc) {
        this.iddanhmuc = iddanhmuc;
    }

    public String getLkanh() {
        return lkanh;
    }

    public void setLkanh(String lkanh) {
        this.lkanh = lkanh;
    }
}
