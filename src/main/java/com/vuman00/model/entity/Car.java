package com.vuman00.model.entity;


import com.vuman00.model.entity.Enum.Brand;

public abstract class Car {
    private int idcar;
    private String carname;
    private  String numberPlate;
    private  int yearManufacture;
    private Brand brand;
    private  boolean haveIsurance;

    public int getIdcar() {
        return idcar;
    }

    public void setIdcar(int idcar) {
        this.idcar = idcar;
    }

    public String getCarname() {
        return carname;
    }

    public void setCarname(String carname) {
        this.carname = carname;
    }

    public String getNumberPlate() {
        return numberPlate;
    }

    public void setNumberPlate(String numberPlate) {
        this.numberPlate = numberPlate;
    }

    public int getYearManufacture() {
        return yearManufacture;
    }

    public void setYearManufacture(int yearManufacture) {
        this.yearManufacture = yearManufacture;
    }

    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public boolean isHaveIsurance() {
        return haveIsurance;
    }

    public void setHaveIsurance(boolean haveIsurance) {
        this.haveIsurance = haveIsurance;
    }

}
