package com.vuman00.model.entity;

public class OldCar extends Car {
    private int actionDuration;

    public int getActionDuration() {
        return actionDuration;
    }

    public void setActionDuration(int actionDuration) {
        this.actionDuration = actionDuration;
    }

}
