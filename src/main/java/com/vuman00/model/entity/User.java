package com.vuman00.model.entity;

public class User {
    private String username;
    private String password;
    private String name;
    private int role;
    private int biensoxe;


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public int getBiensoxe() {
        return biensoxe;
    }

    public void setBiensoxe(int biensoxe) {
        this.biensoxe = biensoxe;
    }
}
