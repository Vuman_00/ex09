package com.vuman00.model.entity.Enum;

public enum Brand {
    TOYOTA(0,"Toyota"),BMW(1,"BMW"),HUYNDAI(2,"Huyndai");
   private int ma;
   private String ten;

    Brand(int ma, String ten) {
        this.ma = ma;
        this.ten = ten;
    }

    public int getMa() {
        return ma;
    }

    public void setMa(int ma) {
        this.ma = ma;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }
}
