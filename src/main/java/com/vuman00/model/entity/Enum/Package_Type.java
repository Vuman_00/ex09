package com.vuman00.model.entity.Enum;

public enum Package_Type {
    A("Đời mới"),B("Đời trung"),C("Đời cũ");
    String tengoibaohiem;

    Package_Type(String tengoibaohiem) {
        this.tengoibaohiem = tengoibaohiem;
    }

    public String getTengoibaohiem() {
        return tengoibaohiem;
    }

    public void setTengoibaohiem(String tengoibaohiem) {
        this.tengoibaohiem = tengoibaohiem;
    }
}
