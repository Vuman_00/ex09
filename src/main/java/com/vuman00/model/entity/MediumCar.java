package com.vuman00.model.entity;

public class MediumCar extends Car {


    private  boolean havPowersteering;

    public boolean isHavPowersteering() {
        return havPowersteering;
    }

    public void setHavPowersteering(boolean havPowersteering) {
        this.havPowersteering = havPowersteering;
    }

}
